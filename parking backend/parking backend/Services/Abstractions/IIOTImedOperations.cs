using System.Threading;

namespace parking_backend.Services.Abstractions
{
    public interface IIoTimedOperations
    {
        void SetTransactionTimer(double countSecondsPerTeak, TimerCallback callback);
        void ResetTransactionTimer();
        void SetTransactionLogWriter(TimerCallback callback);
        void ResetTransactionLogWriter();
    }
}