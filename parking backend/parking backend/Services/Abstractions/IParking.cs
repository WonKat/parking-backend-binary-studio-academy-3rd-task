using System;
using System.Collections.Generic;
using parking_backend.Models;

namespace parking_backend.Services.Abstractions
{
    public interface IParking
    {
        double Balance { get; set; }
        Tuple<int, int> GetInforationAboutFreeOccupiedPlaces();
        List<Transaction> GetAllTransactionsForLastMinute();
        List<string> GetAllTransactionsHistory();
        List<Transport> GetAllAvailableTransport();
        void AddNewTransport(Transport transport);
        void RemoveExistTransport(int transportId);
        void ReplenishBalanceForExistTransport(int transportId, int sum);
        void WriteTransactionsToFileAndClearCollection(object obj);
        void GetTaxesFromTransport(object obj);
        double GetSalaryForLastMinute();
        bool IsTransportWithSpecificIdOlreadyExist(int Id);
        bool IsParkingEmpty();

        void SetBasicConfiguration(double startBalance, int maxParkingSpaciousness, double periodForPaymentsInSeconds,
            double taxCoefficient);
    }
}