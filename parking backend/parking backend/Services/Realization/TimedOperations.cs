using System;
using System.Threading;
using parking_backend.Services.Abstractions;

namespace parking_backend.Services.Realization
{
    public class TimedOperations:IIoTimedOperations
    {
        private Timer _transactionTimer;
        private Timer _transactionLogWriter;
        private TimeSpan _spanForLogWriter = TimeSpan.FromMinutes(1);
        public void SetTransactionTimer(double countSecondsPerTeak, TimerCallback callback)
        {
            _transactionTimer = new Timer(callback,null,TimeSpan.Zero, 
                TimeSpan.FromSeconds(countSecondsPerTeak));
        }

        public void ResetTransactionTimer()
        {
            _transactionLogWriter = null;
        }

        public void SetTransactionLogWriter(TimerCallback callback)
        {
            
            _transactionLogWriter = new Timer(callback,null,TimeSpan.Zero, 
                _spanForLogWriter);
        }

        public void ResetTransactionLogWriter()
        {
            _transactionLogWriter = null;
        }
    }
}