﻿using System;

namespace parking_backend.Exceptions
{
    class FullParkingException:Exception
    {
        public FullParkingException()
        {
            
        }
        public FullParkingException(string message):base(message)
        {
            
        }
    }
}
