using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using parking_backend.Exceptions;
using parking_backend.Models;
using parking_backend.Services.Abstractions;

namespace parking_backend.Controllers
{
    [Route("/api/[controller]")]
    public class ParkingController:Controller
    {
        private readonly IParking _parking;

        public ParkingController(IParking parking)
        {
            _parking = parking;
        }

        [HttpGet("info/balance")]
        public IActionResult GetCurrentBalance()
        {
            return Ok(_parking.Balance);
        }

        [HttpGet("info/places")]
        public IActionResult GetInforationAboutFreeOccupiedPlaces()
        {
            return Ok(_parking.GetInforationAboutFreeOccupiedPlaces());
        }
        

        [HttpGet("transactions/last")]
        public IActionResult GetAllTransactionsForLastMinute()
        {
            return Ok(_parking.GetAllTransactionsForLastMinute());
        }

        [HttpGet("transactions/last/salary")]
        public IActionResult GetSalaryForLastMinute()
        {
            return Ok(_parking.GetSalaryForLastMinute());
        }
        [HttpGet("transactions")]
        public IActionResult GetAllTransactionsHistory()
        {
            try
            {
                return Ok(_parking.GetAllTransactionsHistory());
            }
            catch (FileNotFoundException )
            {
                return NoContent();
            }
        }

        [HttpGet("transport")]
        public IActionResult GetAllAvailableTransport()
        {
            return Ok(_parking.GetAllAvailableTransport());
        }
        [HttpPost("transport")]
        public IActionResult AddNewTransport([FromBody,Required] Transport transport)
        {
            try
            {
                _parking.AddNewTransport(transport);
                return Ok();
            }
            catch (FullParkingException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("transport")]
        public IActionResult DeleteExistTransport([FromQuery,Required] int id)
        {
            if (_parking.IsParkingEmpty())
                return NoContent();
            try
            {
                _parking.RemoveExistTransport(id);
                return Ok();
            }
            catch (InvalidOperationException )
            {
                return BadRequest("Parking haven't transport with same id");                                
            }
        }

        [HttpPut("transport")]
        public IActionResult ReplenishBalanceForExistTransport([FromQuery, Required] int id,
            [FromQuery, Required] int sum)
        {
            if (_parking.IsParkingEmpty())
                return NoContent();
            try
            {
                _parking.ReplenishBalanceForExistTransport(id,sum);
                return Ok();
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Parking haven't transport with same id");
            }
        }

        [HttpGet("transport/exist")]
        public IActionResult IsTransportWithSpecificIdOlreadyExist([Required, FromQuery] int id)
        {
            if (_parking.IsTransportWithSpecificIdOlreadyExist(id))
                return Ok();
            return NoContent();
        }
    }
}