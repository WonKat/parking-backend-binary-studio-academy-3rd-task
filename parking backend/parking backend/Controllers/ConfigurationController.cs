using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using parking_backend.Models;
using parking_backend.Models.RequestBodyObjects;
using parking_backend.Services.Abstractions;

namespace parking_backend.Controllers
{
    [Route("/api/[controller]")]
    public class ConfigurationController:Controller
    {
        private readonly IParking _parking;
        private readonly IIoTimedOperations _timedOperations;
        public ConfigurationController(IParking parking, IIoTimedOperations timedOperations)
        {
            _parking = parking;
            _timedOperations = timedOperations;
        }
        [HttpPost("set")]
        public IActionResult SetBasicConfigurations([FromBody,Required] ConfigurationData configurations)
        {
            _parking.SetBasicConfiguration(configurations.StartParkingBalance,configurations.MaxParkingSpaciousness,
                configurations.PeriodForPaymentsInSeconds,configurations.TaxCoefficient);
            _timedOperations.SetTransactionTimer(configurations.PeriodForPaymentsInSeconds,
                _parking.GetTaxesFromTransport);
            _timedOperations.SetTransactionLogWriter(_parking.WriteTransactionsToFileAndClearCollection);
            return Ok();
        }

        [HttpDelete("timer/reset")]
        public IActionResult DeleteTimers()
        {
            _timedOperations.ResetTransactionTimer();
            _timedOperations.ResetTransactionLogWriter();
            return Ok();
        }
    }
}