namespace parking_backend.Models
{
    public enum TransportTypes
    {
        Car=1,
        Truck,
        Bus,
        Motorcycle
    }
}