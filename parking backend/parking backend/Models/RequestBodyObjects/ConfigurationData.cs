namespace parking_backend.Models.RequestBodyObjects
{
    public class ConfigurationData
    {
        public double StartParkingBalance { get; set; }
        public int MaxParkingSpaciousness { get; set; }
        public double PeriodForPaymentsInSeconds { get; set; }
        public double TaxCoefficient { get; set; }
    }
}