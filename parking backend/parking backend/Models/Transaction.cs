﻿using System;

namespace parking_backend.Models
{
    public class Transaction
    {
        public DateTime Time { get; set; }
        public Transport Transport { get; set;}
        public double Payment { get; set; }

        public override string ToString()
        {
            return $"Time : {Time} TransportId : {Transport.Id} " +
                   $"Driver name : {Transport.DriverName} Payment : {Payment}";
        }
    }
}
